﻿namespace DescartesTask.Service.Options
{
    public class DbOptions
    {
        public const string CONNECTION_STRINGS = "ConnectionStrings";

        public string DatabaseConnectionStringLocal { get; set; }
        public string DatabaseConnectionStringLive { get; set; }

        public bool UseLiveDb { get; set; }
    }
}
