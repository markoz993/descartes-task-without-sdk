﻿using DescartesTask.Domain.DTOs;
using System.Collections.Generic;

namespace DescartesTask.Service.ViewModels
{
    public class DiffResultDto
    {
        public int Id { get; set; }

        public InputDto Left { get; set; }
        public InputDto Right { get; set; }

        public string DiffResultType { get; set; }

        public IEnumerable<DiffDto> Diffs { get; set; }
    }
}