﻿namespace DescartesTask.Service.ViewModels
{
    public class DiffDto
    {
        public int Offset { get; set; }
        public int Length { get; set; }
    }
}
