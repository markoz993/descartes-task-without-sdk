﻿using DescartesTask.Service.ServicesAbstract;

namespace DescartesTask.Service.Services
{
    public class ValidationService : IValidationService
    {
        public bool ValidateId(int id)
        {
            if (id < 1)
                return false;

            return true;
        }

        public bool ValidatePayload(string data)
        {
            if (string.IsNullOrWhiteSpace(data))
                return false;

            return true;
        }
    }
}
