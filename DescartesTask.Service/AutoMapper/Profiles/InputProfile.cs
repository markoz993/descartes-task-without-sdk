﻿using DescartesTask.Domain.Entities;
using DescartesTask.Domain.DTOs;
using AutoMapper;

namespace DescartesTask.Service.AutoMapper.Profiles
{
    public class InputProfile : Profile
    {
        public InputProfile()
        {
            CreateMap<Input, InputDto>().ReverseMap();
        }
    }
}
