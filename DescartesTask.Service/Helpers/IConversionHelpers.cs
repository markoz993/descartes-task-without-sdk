﻿namespace DescartesTask.Service.Helpers
{
    public interface IConversionHelpers
    {
        string Base64Encode(string data);

        string Base64Decode(string data);
    }
}
