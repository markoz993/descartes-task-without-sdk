﻿using System.Text;
using System;

namespace DescartesTask.Service.Helpers
{
    public class ConversionHelpers : IConversionHelpers
    {
        public string Base64Decode(string data)
        {
            if (string.IsNullOrWhiteSpace(data))
                return string.Empty;

            var plainTextBytes = Encoding.UTF8.GetBytes(data);
            return Convert.ToBase64String(plainTextBytes);
        }

        public string Base64Encode(string data)
        {
            if (string.IsNullOrWhiteSpace(data))
                return string.Empty;

            var base64EncodedBytes = Convert.FromBase64String(data);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
