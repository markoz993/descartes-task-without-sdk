﻿using DescartesTask.Sdk.Authentication;
using DescartesTask.Sdk.Decompression;
using DescartesTask.Sdk.Util;
using DescartesTask.Sdk;
using System.Net.Http;
using System;

namespace DescartesTask.IntegrationTests
{
    public static class TestSetup
    {
        public static ApiClient GetLocalClient()
        {
            const string apiUrl = "https://localhost:44314/";

            return ConstructClient(apiUrl);
        }

        private static ApiClient ConstructClient(string calendarApiUrl)
        {
            var httpClientSignIn = new HttpClient(new DefaultHttpClientHandler())
            {
                BaseAddress = new Uri(calendarApiUrl)
            };
            var tokenHandler = new ClientCredentialsTokenHandler(httpClientSignIn, new ClientCredentialsOptions
            {
                ClientId = TestConfig.ClientId,
                ClientSecret = TestConfig.ClientSecret
            });

            var httpClient = new HttpClient(new DefaultHttpClientHandler()) { BaseAddress = new Uri(calendarApiUrl) };

            var options = new ClientOptions
            {
                Compressor = new GZipCompressor(),
                GetToken = async () => await tokenHandler.GetBearerTokenAsync()
            };

            return new ApiClient(httpClient, options);
        }
    }
}
