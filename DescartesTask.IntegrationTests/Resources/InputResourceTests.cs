﻿using DescartesTask.IntegrationTests.Tests;
using System.Threading.Tasks;
using Xunit;

namespace DescartesTask.IntegrationTests.Resources
{
    public class InputResourceTests
    {
        private readonly InputTests _tests;

        public InputResourceTests()
        {
            _tests = new InputTests(TestSetup.GetLocalClient());
        }

        [Fact]
        [Trait("Execute", "OnBuildServer")]
        public async Task TestPutLeft()
        {
            await _tests.TestPutLeft();
        }

        [Fact]
        [Trait("Execute", "OnBuildServer")]
        public async Task TestPutRight()
        {
            await _tests.TestPutRight();
        }

        [Fact]
        [Trait("Execute", "OnBuildServer")]
        public async Task TestDiff()
        {
            await _tests.TestDiff();
        }
    }
}
