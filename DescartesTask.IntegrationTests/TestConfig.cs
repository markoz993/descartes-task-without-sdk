﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace DescartesTask.IntegrationTests
{
    public static class TestConfig
    {
        public static string ClientId { get; set; }
        public static string ClientSecret { get; set; }

        static TestConfig()
        {
            var config = new ConfigurationBuilder()
                .AddInMemoryCollection(new Dictionary<string, string> {
                    { "ClientId", "DescartesTask" },
                    { "ClientSecret", "SECRET_PLACEHOLDER" }
                }).Build();

            ClientId = config["ClientId"];
            ClientSecret = config["ClientSecret"];
        }
    }
}