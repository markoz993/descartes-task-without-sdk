﻿using DescartesTask.Domain.Entities.Enums;

namespace DescartesTask.Domain.Filters
{
    public class InputFilter : BaseFilter
    {
        public InputFilter()
        {
            OrderBy = "TimeCreated";
        }

        public int OrderId { get; set; }

        public string SearchTerm { get; set; }

        public InputTypeEnum Type { get; set; }
    }
}
