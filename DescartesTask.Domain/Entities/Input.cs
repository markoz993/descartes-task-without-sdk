﻿using System;

namespace DescartesTask.Domain.Entities
{
    public class Input
    {
        public Guid Id { get; set; }

        public int OrderingId { get; set; }

        public DateTime TimeCreated { get; set; }

        public string Data { get; set; }

        public int TypeId { get; set; }
    }
}
