﻿using DescartesTask.Domain.Entities.Enums;
using DescartesTask.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DescartesTask.Domain.Configuration
{
    public class InputConfiguration : IEntityTypeConfiguration<Input>
    {
        public void Configure(EntityTypeBuilder<Input> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne<InputType>().WithMany().HasForeignKey(x => x.TypeId).OnDelete(DeleteBehavior.Restrict);

            builder.Property(x => x.Data).HasMaxLength(10240).IsRequired();
        }
    }
}