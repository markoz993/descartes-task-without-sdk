﻿using DescartesTask.Domain.Configuration.Enum;
using DescartesTask.Domain.Entities.Enums;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace DescartesTask.Domain.Extensions
{
    internal static class ModelBuilderEnumExtensions
    {
        internal static void ApplyEnumConfiguration(this ModelBuilder modelBuilder)
        {
            ApplyInputTypeConfiguration(modelBuilder);

            modelBuilder.ApplyConfiguration(new InputTypeConfiguration());
        }

        private static void ApplyInputTypeConfiguration(ModelBuilder modelBuilder)
        {
            foreach (var reference in Enum.GetValues(typeof(InputTypeEnum)).Cast<InputTypeEnum>())
            {
                modelBuilder.Entity<InputType>().HasData(new InputType
                {
                    Id = (int)reference,
                    Name = reference.ToString(),
                    IsDeleted = false
                });
            }
        }
    }
}