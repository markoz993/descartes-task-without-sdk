﻿using DescartesTask.Sdk.Contracts;
using DescartesTask.Sdk.Retry;
using DescartesTask.Sdk;
using System.Threading.Tasks;
using System.Net;
using System;
using Xunit;

namespace DescartesTask.UnitTests.Retry
{
    public class RetryTests
    {
        private readonly TimeSpan[] retryDelays = {
            TimeSpan.FromMilliseconds(500),
            TimeSpan.FromSeconds(1),
            TimeSpan.FromSeconds(2)
        };

        [Fact]
        public async Task TestSuccessOnSecondTry()
        {
            int retry = 0;

            var policy = new RetryWithTimeSpanBackoff(retryDelays);
            var result = await policy.RunAsync(() => {
                ++retry;
                //Return error first try
                if (retry == 1)
                    return Task.FromResult(new ApiResult<int>(HttpStatusCode.InternalServerError, new ErrorDto { IsRetryable = true }));

                //Return success second
                return Task.FromResult(new ApiResult<int>(HttpStatusCode.OK, 1));
            });

            Assert.Equal(1, result.Data);
        }

        [Fact]
        public async Task TestNeverSuccess()
        {
            int retry = 0;

            var policy = new RetryWithTimeSpanBackoff(retryDelays);
            var result = await policy.RunAsync(() => {
                ++retry;
                return Task.FromResult(new ApiResult<int>(HttpStatusCode.InternalServerError, new ErrorDto
                {
                    IsRetryable = true
                }));
            });

            Assert.Equal(0, result.Data);
            Assert.Equal(4, retry);
        }

        [Fact]
        public async Task TestNoRetry()
        {
            int retry = 0;

            var policy = new NoRetryPolicy();
            var result = await policy.RunAsync(() => {
                ++retry;
                return Task.FromResult(new ApiResult<int>(HttpStatusCode.InternalServerError, new ErrorDto
                {
                    IsRetryable = true
                }));
            });

            Assert.Equal(0, result.Data);
            Assert.Equal(1, retry);
        }
    }
}