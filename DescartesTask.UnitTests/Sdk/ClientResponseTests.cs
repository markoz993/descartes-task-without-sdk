﻿using DescartesTask.Sdk.Authentication;
using DescartesTask.UnitTests.Utils;
using DescartesTask.Sdk.Contracts;
using DescartesTask.Sdk.Util;
using DescartesTask.Sdk;
using NSubstitute;
using Xunit;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

namespace DescartesTask.UnitTests.Sdk
{
    public class ClientResponseTests
    {
        [Fact]
        public async Task TestEmptyObjectResponse()
        {
            var result = await MakeRequestAsync<DescartesTask.Sdk.Contracts.InputDto>(HttpStatusCode.Accepted, string.Empty);
            Assert.True(result.IsSuccessStatusCode);
            Assert.Null(result.Error);
            Assert.Null(result.Data);
            Assert.Equal(HttpStatusCode.Accepted, result.StatusCode);
        }

        [Fact]
        public async Task TestNoContentResponse()
        {
            var result = await MakeRequestAsync<DescartesTask.Sdk.Contracts.InputDto>(HttpStatusCode.NoContent, string.Empty);
            Assert.True(result.IsSuccessStatusCode);
            Assert.Null(result.Error);
            Assert.Null(result.Data);
            Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);
        }

        [Fact]
        public async Task TestServiceDown()
        {
            var result = await MakeRequestAsync<DescartesTask.Sdk.Contracts.InputDto>(HttpStatusCode.ServiceUnavailable, string.Empty);
            Assert.False(result.IsSuccessStatusCode);
            Assert.Equal(ErrorCodes.EndpointUnavailable, result.Error?.ErrorCode);
            Assert.Null(result.Data);
            Assert.Equal(HttpStatusCode.ServiceUnavailable, result.StatusCode);
        }

        [Fact]
        public async Task TestEmptyIntResponse()
        {
            var result = await MakeRequestAsync<int>(HttpStatusCode.Accepted, string.Empty);
            Assert.True(result.IsSuccessStatusCode);
            Assert.Null(result.Error);
            Assert.Equal(0, result.Data);
            Assert.Equal(HttpStatusCode.Accepted, result.StatusCode);
        }

        [Fact]
        public async Task TestSendingHttpContent()
        {
            var handler = new FakeResponseHttpMessageHandler(HttpStatusCode.Accepted, string.Empty);
            var result = await MakeHttpContentRequest<int>(handler);
            Assert.True(result.IsSuccessStatusCode);
            Assert.Null(result.Error);
            Assert.Equal(0, result.Data);
            Assert.Equal(HttpStatusCode.Accepted, result.StatusCode);
        }

        [Fact]
        public async Task TestErrorNoResponse()
        {
            var result = await MakeRequestAsync<int>(HttpStatusCode.BadRequest, string.Empty);
            Assert.False(result.IsSuccessStatusCode);
            Assert.Equal(ErrorCodes.NoContent, result.Error?.ErrorCode);
            Assert.Equal(0, result.Data);
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        }

        [Fact]
        public async Task TestExceptionFromRequest()
        {
            var result = await MakeRequestAsync<int>(new ExceptionHttpMessageHandler());
            Assert.False(result.IsSuccessStatusCode);
            Assert.Equal(ErrorCodes.EndpointFailiure, result.Error?.ErrorCode);
            Assert.Equal(0, result.Data);
            Assert.Equal(HttpStatusCode.InternalServerError, result.StatusCode);
        }

        [Fact]
        public async Task TestErrorResponse()
        {
            var error = new ErrorDto
            {
                ErrorCode = ErrorCodes.UnknownObjectType,
                Message = "Exected error message"
            };

            var result = await MakeRequestAsync<int>(HttpStatusCode.BadRequest, Newtonsoft.Json.JsonConvert.SerializeObject(error));
            Assert.False(result.IsSuccessStatusCode);
            Assert.Equal(error.ErrorCode, result.Error?.ErrorCode);
            Assert.Equal(error.Message, result.Error?.Message);
            Assert.Equal(0, result.Data);
        }

        [Theory]
        [InlineData(HttpStatusCode.OK)]
        [InlineData(HttpStatusCode.BadRequest)]
        public async Task TestUnableToSerializeErrorResponseWhenSuccess(HttpStatusCode statusCode)
        {
            var result = await MakeRequestAsync<int>(statusCode, "<html><body>String that is not json</body></html>");
            Assert.Equal(((int)statusCode >= 200) && (int)statusCode <= 299, result.IsSuccessStatusCode);
            Assert.Equal(statusCode, result.StatusCode);
            Assert.Equal(ErrorCodes.UnableToSerializeResponse, result.Error?.ErrorCode);
            Assert.Equal(0, result.Data);
        }

        [Fact]
        public async Task TestMakeRequestAndFailToFetchToken()
        {
            var result = await MakeRequestAndFailToFetchTokenAsync<int>(HttpStatusCode.OK, "{ Test: true }", HttpStatusCode.BadRequest, new TokenResponseDto { Error = "Unable to get token" });
            Assert.False(result.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            Assert.Equal(0, result.Data);
        }

        private static async Task<ApiResult<T>> MakeRequestAsync<T>(HttpMessageHandler messageHandler)
        {
            var httpClient = TestHttpClientFactory.GetFakeHttpClientWithMessage(messageHandler);

            var tokenHandler = Substitute.For<IBearerTokenHandler>();
            tokenHandler
                .GetBearerTokenAsync()
                .ReturnsForAnyArgs(Task.FromResult(new ApiResult<string>(HttpStatusCode.OK, "MockedToken")));

            var client = new ApiClient(httpClient, new ClientOptions { GetToken = async () => await tokenHandler.GetBearerTokenAsync() });
            var result = await client.PutAsync<T>(new RequestOptions(), "/api/v1/dummyendpoint/", null);

            return result;
        }

        private static async Task<ApiResult<T>> MakeHttpContentRequest<T>(HttpMessageHandler messageHandler)
        {
            var httpClient = TestHttpClientFactory.GetFakeHttpClientWithMessage(messageHandler);

            var tokenHandler = Substitute.For<IBearerTokenHandler>();
            tokenHandler
                .GetBearerTokenAsync()
                .ReturnsForAnyArgs(Task.FromResult(new ApiResult<string>(HttpStatusCode.OK, "MockedToken")));

            var client = new ApiClient(httpClient, new ClientOptions()
            {
                GetToken = async () => { return await tokenHandler.GetBearerTokenAsync(); }
            });
            var result = await client.PutAsync<T>(new RequestOptions(), "/api/v1/dummyendpoint/", new StringContent("test"));

            return result;
        }

        private static async Task<ApiResult<T>> MakeRequestAsync<T>(HttpStatusCode statusCode, string content)
        {
            var handler = new FakeResponseHttpMessageHandler(statusCode, content);

            return await MakeRequestAsync<T>(handler);
        }

        private static async Task<ApiResult<T>> MakeRequestAndFailToFetchTokenAsync<T>(
            HttpStatusCode statusCode, string content, HttpStatusCode tokenStatusCode, TokenResponseDto tokenResponse)
        {
            var handler = new FakeResponseHttpMessageHandler(statusCode, content);
            var httpClient = TestHttpClientFactory.GetFakeHttpClientWithMessage(handler);

            var tokenHandler = Substitute.For<IBearerTokenHandler>();
            tokenHandler
                .GetBearerTokenAsync()
                .ReturnsForAnyArgs(Task.FromResult(new ApiResult<string>(tokenStatusCode, tokenResponse?.AccessToken)));

            var client = new ApiClient(httpClient, new ClientOptions() { GetToken = async () => await tokenHandler.GetBearerTokenAsync() });
            var result = await client.PutAsync<T>(new RequestOptions(), "/api/v1/dummyendpoint/", null);

            return result;
        }
    }
}