﻿using DescartesTask.Sdk;
using System.Runtime.Serialization;
using System;
using Newtonsoft.Json;
using Xunit;

namespace DescartesTask.UnitTests.Sdk
{
    public class ApiHttpExceptionTests
    {
        [Fact]
        public void TestSerializeException()
        {
            var exception = new ApiHttpException(System.Net.HttpStatusCode.OK, "ErrorCode", "Message");
            var str = JsonConvert.SerializeObject(exception);
            var deserializedException = JsonConvert.DeserializeObject<ApiHttpException>(str);

            Assert.NotNull(deserializedException);
        }

        [Fact]
        public void TestSerializationInfoNull()
        {
            Assert.Throws<ArgumentNullException>(() => {
                var exception = new ApiHttpException(System.Net.HttpStatusCode.OK, "ErrorCode", "Message");
                exception.GetObjectData(null, new StreamingContext());
            });
        }
    }
}
