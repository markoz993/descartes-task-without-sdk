﻿using DescartesTask.Sdk;
using System.Threading.Tasks;
using System.Net;
using Xunit;

namespace DescartesTask.UnitTests.Sdk
{
    public class ApiRequestTests
    {
        [Fact]
        public async Task TestResultConstructorAsync()
        {
            var request = new ApiRequest<bool>(options => {
                return Task.FromResult(new ApiResult<bool>(HttpStatusCode.OK, true));
            });

            Assert.True(await request.ExecAsync());
        }
    }
}
