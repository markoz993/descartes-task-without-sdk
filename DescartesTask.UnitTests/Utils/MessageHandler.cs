﻿using System.Threading.Tasks;
using System.Threading;
using System.Net.Http;
using System.Net;
using System;

namespace DescartesTask.UnitTests.Utils
{
    internal class ExceptionHttpMessageHandler : HttpMessageHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            throw new HttpRequestException("Service not responding");
        }
    }

    internal class FakeResponseHttpMessageHandler : HttpMessageHandler
    {
        public string Content { get; set; }
        public HttpStatusCode StatusCode { get; set; }

        public FakeResponseHttpMessageHandler(HttpStatusCode statusCode, string content)
        {
            StatusCode = statusCode;
            Content = content;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.FromResult(CreateResponseMessage(StatusCode, Content));
        }

        private static HttpResponseMessage CreateResponseMessage(HttpStatusCode statusCode, string content)
        {
            return new HttpResponseMessage
            {
                StatusCode = statusCode,
                Content = new StringContent(content),
                RequestMessage = new HttpRequestMessage { RequestUri = new Uri("https://localhost/") }
            };
        }
    }
}
