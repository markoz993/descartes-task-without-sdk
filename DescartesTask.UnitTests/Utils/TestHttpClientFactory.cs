﻿using System.Net.Http;
using System;

namespace DescartesTask.UnitTests.Utils
{
    public class TestHttpClientFactory
    {
        public static HttpClient GetFakeHttpClientWithMessage(HttpMessageHandler httpMessageHandler)
        {
            var client = new HttpClient(httpMessageHandler)
            {
                BaseAddress = new Uri("https://localhost/")
            };

            return client;
        }
    }
}
