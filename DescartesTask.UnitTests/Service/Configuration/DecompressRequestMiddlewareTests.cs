﻿using DescartesTask.Api.Middleware;
using NSubstitute;
using Xunit;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.IO.Compression;
using System.Net;
using System.IO;

namespace DescartesTask.UnitTests.Service.Configuration
{
    public class DecompressRequestMiddlewareTests
    {
        public static int Counter = 0;

        [Fact]
        public async Task TestNoEncoding()
        {
            var logMiddleware = new DecompressRequestMiddleware(DelegateMethod);
            var httpContext = MockHttpContext("/test/", HttpStatusCode.OK);
            httpContext.Request.Body = new MemoryStream();

            Counter = 0;
            await logMiddleware.Invoke(httpContext);

            Assert.Equal(typeof(MemoryStream), httpContext.Request.Body.GetType());
            Assert.Equal(1, Counter);
        }

        [Fact]
        public async Task TestUnkownEncoding()
        {
            var logMiddleware = new DecompressRequestMiddleware(DelegateMethod);
            var httpContext = MockHttpContext("/test/", HttpStatusCode.OK);
            httpContext.Request.Body = new MemoryStream();
            httpContext.Request.Headers.Add("Content-Encoding", "dummy_encoding");

            Counter = 0;
            await logMiddleware.Invoke(httpContext);

            Assert.Equal(typeof(MemoryStream), httpContext.Request.Body.GetType());
            Assert.Equal(1, Counter);
        }

        [Fact]
        public async Task TestDeflateEncoding()
        {
            var logMiddleware = new DecompressRequestMiddleware(DelegateMethod);
            var httpContext = MockHttpContext("/test/", HttpStatusCode.OK);
            httpContext.Request.Body = new MemoryStream();
            httpContext.Request.Headers.Add("Content-Encoding", "deflate");

            Counter = 0;
            await logMiddleware.Invoke(httpContext);

            Assert.Equal(typeof(DeflateStream), httpContext.Request.Body.GetType());
            Assert.Equal(1, Counter);
        }

        [Fact]
        public async Task TestBrotliEncoding()
        {
            var logMiddleware = new DecompressRequestMiddleware(DelegateMethod);
            var httpContext = MockHttpContext("/test/", HttpStatusCode.OK);
            httpContext.Request.Body = new MemoryStream();
            httpContext.Request.Headers.Add("Content-Encoding", "br");

            Counter = 0;
            await logMiddleware.Invoke(httpContext);

            Assert.Equal(typeof(BrotliStream), httpContext.Request.Body.GetType());
            Assert.Equal(1, Counter);
        }

        private HttpContext MockHttpContext(string path, HttpStatusCode statusCode)
        {
            var request = new DefaultHttpContext().Request;
            request.Scheme = "http";
            request.Host = new HostString("my.HoΨst:80");
            request.PathBase = new PathString(path);
            request.Path = new PathString("/");
            request.QueryString = new QueryString("?name=val%23ue");
            request.Method = "GET";

            var session = Substitute.For<ISession>();
            var httpResponse = Substitute.For<HttpResponse>();
            var accessor = Substitute.For<IHttpContextAccessor>();
            accessor.HttpContext.Request.Returns(request);
            accessor.HttpContext.Session.Returns(session);
            httpResponse.StatusCode = (int)statusCode;
            accessor.HttpContext.Response.Returns(httpResponse);

            return accessor.HttpContext;
        }

        private static Task<HttpContext> DelegateMethod(HttpContext message)
        {
            ++Counter;
            return Task.FromResult(message);
        }
    }
}