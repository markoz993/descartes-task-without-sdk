﻿using DescartesTask.Service.ServicesAbstract;
using DescartesTask.Api.Controllers.v1;
using DescartesTask.Sdk.Contracts;
using DescartesTask.Sdk.Util;
using DescartesTask.Sdk;
using NSubstitute;
using Xunit;
using System.Threading.Tasks;
using System.Net;

namespace DescartesTask.UnitTests.Service.Api
{
    public class InputsControllerTests
    {
        //Just in case if we needed mapping
        //readonly IMapper mapper = AutoMapperConfiguration.Configure();

        [Fact]
        public async Task WhenPutLeftIsCalled_ReturnTrueResult()
        {
            var validator = GetValidatorSubstitute();
            var service = GetInputServiceSubstitute();
            service.SaveLeft(Arg.Any<int>(), Arg.Any<string>()).Returns(_ => true);

            var controller = new InputsController(service, validator);
            var result = await controller.PutLeft(3, "AAAAAAA==");

            Assert.True(result);
        }

        [Fact]
        public async Task WhenPutRightIsCalled_ReturnTrueResult()
        {
            var validator = GetValidatorSubstitute();
            var service = GetInputServiceSubstitute();
            service.SaveRight(Arg.Any<int>(), Arg.Any<string>()).Returns(_ => true);

            var controller = new InputsController(service, validator);
            var result = await controller.PutRight(3, "AAAAAAA==");

            Assert.True(result);
        }

        [Fact]
        public async Task WhenGetDiffIsCalled_ReturnEqualsResult()
        {
            var validator = GetValidatorSubstitute();
            var service = GetInputServiceSubstitute();
            service.GetDiff(Arg.Any<int>()).Returns(_ => new DiffResultDto { DiffResultType = "Equals" });

            var controller = new InputsController(service, validator);
            var result = await controller.GetDiff(3);

            Assert.Equal("Equals", result.DiffResultType);
        }

        [Fact]
        public async Task WhenGetDiffIsCalledForInvalidId_ReturnException()
        {
            var validator = GetValidatorSubstitute();
            var service = GetInputServiceSubstitute();
            service.GetDiff(0).Returns(_ => new DiffResultDto());

            var controller = new InputsController(service, validator);
            var exception = await Assert.ThrowsAsync<ApiHttpException>(() => controller.GetDiff(0));

            Assert.Equal(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.NotNull(exception.Error);
            Assert.Equal(ErrorCodes.BadRequest, exception.Error.ErrorCode);
            Assert.Equal("Id provided is not valid.", exception.Message);
        }

        private IInputService GetInputServiceSubstitute() =>
            Substitute.For<IInputService>();

        private IValidationService GetValidatorSubstitute() => 
            Substitute.For<IValidationService>();
    }
}