﻿using DescartesTask.Api.Controllers;
using DescartesTask.Domain.Entities;
using DescartesTask.Sdk.Contracts;
using DescartesTask.Sdk.Util;
using DescartesTask.Domain;
using NSubstitute;
using Xunit;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using System.Security.Authentication;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System;

namespace DescartesTask.UnitTests.Service.Api
{
    public class CustomExceptionFilterAttributeTests
    {
        [Fact]
        public void TestAuthenticationException()
        {
            //Arrange
            var exception = new AuthenticationException();
            var logger = Substitute.For<ILogger<CustomExceptionFilterAttribute>>();
            var attribute = new CustomExceptionFilterAttribute(logger);
            var context = MockExceptionContext(exception);

            //Execute            
            attribute.OnException(context);

            //Assert
            var result = (ObjectResult)context.Result;
            var error = (ErrorDto)(result).Value;

            Assert.Equal(ErrorCodes.NotAuthenticated, error.ErrorCode);
            Assert.Equal(HttpStatusCode.Unauthorized, (HttpStatusCode)result.StatusCode);
        }

        [Fact]
        public async Task TestErrorMessageForDatabaseError()
        {
            //Arrange
            var exception = await Assert.ThrowsAsync<DbUpdateException>(async () => {
                using var db = CreateContextForSQLite();
                db.Inputs.Update(
                   new Input
                   {
                       TimeCreated = DateTime.Now
                   });

                await db.SaveChangesAsync();
            });

            var logger = Substitute.For<ILogger<CustomExceptionFilterAttribute>>();
            var attribute = new CustomExceptionFilterAttribute(logger);
            var context = MockExceptionContext(exception);

            //Execute            
            attribute.OnException(context);

            //Assert
            var result = (ObjectResult)context.Result;
            var error = (ErrorDto)(result).Value;

            Assert.Equal(ErrorCodes.DatabaseError, error.ErrorCode);
            Assert.Equal(HttpStatusCode.InternalServerError, (HttpStatusCode)result.StatusCode);
        }

        private ExceptionContext MockExceptionContext(Exception exception)
        {
            var routeData = new Microsoft.AspNetCore.Routing.RouteData();
            var action = new Microsoft.AspNetCore.Mvc.Abstractions.ActionDescriptor();
            var actionContext = new ActionContext(MockHttpContext(), routeData, action);

            return new ExceptionContext(actionContext, new List<IFilterMetadata> { })
            {
                HttpContext = MockHttpContext(),
                Exception = exception
            };
        }

        private HttpContext MockHttpContext()
        {
            var httpRequest = Substitute.For<HttpRequest>();
            var session = Substitute.For<ISession>();
            var httpResponse = Substitute.For<HttpResponse>();
            var accessor = Substitute.For<IHttpContextAccessor>();
            accessor.HttpContext.Request.Returns(httpRequest);
            accessor.HttpContext.Session.Returns(session);
            accessor.HttpContext.Response.Returns(httpResponse);
            return accessor.HttpContext;
        }

        private RepositoryContext CreateContextForSQLite()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var context = new RepositoryContext(new DbContextOptionsBuilder<RepositoryContext>().Options);

            if (context != null)
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }

            return context;
        }
    }
}