using DescartesTask.Service.AutoMapper;
using DescartesTask.Api.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace DescartesTask.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var mapper = AutoMapperConfiguration.Configure();
            services.AddSingleton(mapper);

            //Cookies
            services.Configure<CookiePolicyOptions>(options => { options.MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.None; });

            services.ConfigureCors();
            services.ConfigureServerIntegration();
            services.ConfigureSqlContext(Configuration);
            services.ConfigureRepositoryManager();

            services.AddRepositories();
            services.AddServices();
            services.AddOptions(Configuration);
            services.AddHelpers(Configuration);

            services.AddHttpContextAccessor(); 
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();

            services.AddControllers(config =>
            {
                config.RespectBrowserAcceptHeader = true;
                config.ReturnHttpNotAcceptable = true;
                config.CacheProfiles.Add("120SecondsDuration", new CacheProfile { Duration = 120 });
            }).AddNewtonsoftJson();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();

            app.UseAuthentication();
            app.UseHttpsRedirection();

            app.UseRequestLocalization();
            app.UseStaticFiles(new StaticFileOptions()
            {
                OnPrepareResponse =
                r =>
                {
                    var path = r.File.PhysicalPath;
                    if (!path.EndsWith(".css") && !path.EndsWith(".js") && !path.EndsWith(".png") &&
                        !path.EndsWith(".jpg") && !path.EndsWith(".ico") && !path.EndsWith(".jpeg"))
                        return;

                    var maxAge = new TimeSpan(7, 0, 0, 0);
                    r.Context.Response.Headers.Append("Cashe-Control", "max-age=" + maxAge.TotalSeconds.ToString("0"));
                }
            });
            app.UseCookiePolicy();
            app.UseCors("CorsPolicy");
            app.UseForwardedHeaders(new ForwardedHeadersOptions { ForwardedHeaders = ForwardedHeaders.All });

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "{controller=Inputs}/{id?}");
            });
        }
    }
}