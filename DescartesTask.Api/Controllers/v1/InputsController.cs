﻿using DescartesTask.Service.ServicesAbstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DescartesTask.Api.Controllers.v1
{
    [Route("v1/diff")]
    [ApiController]
    public class InputsController : ControllerBase
    {
        private readonly IInputService _service;
        private readonly IValidationService _validator;

        public InputsController(IInputService service, IValidationService validator)
        {
            _service = service;
            _validator = validator;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get()
        {
            return Ok("Test");
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetDiff(int id)
        {
            if (!_validator.ValidateId(id))
                return NotFound();

            var result = await _service.GetDiff(id);
            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpPut("{id:int}/left")]
        public async Task<bool> PutLeft(int id, [FromBody] string data)
        {
            if (!_validator.ValidateId(id) || !_validator.ValidatePayload(data))
                return false;

            return await _service.SaveLeft(id, data);
        }

        [HttpPut("{id:int}/right")]
        public async Task<bool> PutRight(int id, [FromBody] string data)
        {
            if (!_validator.ValidateId(id) || !_validator.ValidatePayload(data))
                return false;

            return await _service.SaveRight(id, data);
        }
    }
}