﻿using DescartesTask.Repository.Repositories.Entities;
using DescartesTask.RepositoriesAbstract.Entities;
using DescartesTask.Service.ServicesAbstract;
using DescartesTask.Service.Services;
using DescartesTask.Service.Options;
using DescartesTask.Service.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace DescartesTask.Api.Extensions
{
    public static class StartupDependencyInjectionExtensions
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IInputRepository, InputRepository>();
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddTransient<IInputService, InputService>();
            services.AddTransient<IValidationService, ValidationService>();
        }

        public static void AddOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions<DbOptions>(configuration, DbOptions.CONNECTION_STRINGS);
        }

        public static void AddHelpers(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IConversionHelpers, ConversionHelpers>();
        }

        public static void AddOptions<TOptions>(this IServiceCollection services, IConfiguration configuration, string sectionName) where TOptions : class
        {
            services.Configure<TOptions>(configuration.GetSection(sectionName));
        }
    }
}