﻿using DescartesTask.RepositoriesAbstract.Entities;
using System.Threading.Tasks;

namespace DescartesTask.RepositoriesAbstract
{
    public interface IRepositoryManager
    {
        IInputRepository Inputs { get; }

        Task SaveAsync();
    }
}
