﻿using DescartesTask.RepositoriesAbstract.Entities;
using DescartesTask.Repository.RepoExtensions;
using DescartesTask.Repository.Extensions;
using DescartesTask.Domain.Entities;
using DescartesTask.Domain.Filters;
using DescartesTask.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace DescartesTask.Repository.Repositories.Entities
{
    public class InputRepository : RepositoryBase<Input>, IInputRepository
    {
        public InputRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<Input>> Get(Expression<Func<Input, bool>> predicate, Func<IIncludable<Input>, IIncludable> includeProperties = null, bool trackChanges = false, int count = 0) =>
            count > 0 ?
                await GetEntities(predicate, includeProperties, trackChanges).Take(count).ToListAsync() :
                await GetEntities(predicate, includeProperties, trackChanges).ToListAsync();

        public async Task<IEnumerable<Input>> GetWithFilters(InputFilter filter, Expression<Func<Input, bool>> predicate = null, bool trackChanges = false) =>
            await GetEntities(predicate == null ? x => true : predicate, null, trackChanges)
                .Search(filter.SearchTerm)
                .Sort(filter.OrderBy)
                .ToListAsync();

        public async Task<Input> GetById(Guid id, bool trackChanges = false) =>
            await GetEntities(x => x.Id == id, null, trackChanges).SingleOrDefaultAsync();

        public async Task<int> CountAsync(Expression<Func<Input, bool>> predicate) =>
            await GetEntities(predicate, null, false).CountAsync();

        public void Create(Input brandPerfumer) =>
            CreateEntity(brandPerfumer);

        public void Delete(Input brandPerfumer) =>
            DeleteEntity(brandPerfumer);

        public async Task DeleteRangeAsync(Expression<Func<Input, bool>> predicate)
        {
            var entities = GetEntities(predicate);
            if (entities == null || !await entities.AnyAsync())
                return;

            DeleteRange(entities);
        }
    }
}
