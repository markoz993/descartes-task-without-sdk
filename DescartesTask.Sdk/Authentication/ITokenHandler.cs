﻿using System.Threading.Tasks;

namespace DescartesTask.Sdk.Authentication
{
    public interface ITokenHandler
    {
        Task<ApiResult<TokenResponseDto>> GetTokenAsync();
    }
}
