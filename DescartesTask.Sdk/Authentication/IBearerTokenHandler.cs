﻿using System.Threading.Tasks;

namespace DescartesTask.Sdk.Authentication
{
    public interface IBearerTokenHandler
    {
        Task<ApiResult<string>> GetBearerTokenAsync();
    }
}
