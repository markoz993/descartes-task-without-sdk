﻿using System;

namespace DescartesTask.Sdk.Authentication
{
    public class ClientCredentialsOptions
    {
        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        internal Tuple<DateTimeOffset, ApiResult<TokenResponseDto>> TokenCache { get; set; }
    }
}
