﻿namespace DescartesTask.Sdk.Authentication
{
    public static class TokenResultExtensions
    {
        public static ApiResult<string> ToBearerTokenResult(this ApiResult<TokenResponseDto> result)
        {
            if (!result.IsSuccessStatusCode)
                return new ApiResult<string>(result.StatusCode, result.Error);

            return new ApiResult<string>(result.StatusCode, result.Data?.AccessToken);
        }
    }
}
