﻿using DescartesTask.Sdk.Contracts;
using DescartesTask.Sdk.Util;
using System.Net;

namespace DescartesTask.Sdk
{
    public class ApiResult<T>
    {
        public ApiResult(HttpStatusCode statusCode, T data)
        {
            StatusCode = statusCode;
            Data = data;
        }

        public ApiResult(HttpStatusCode statusCode, ErrorDto error)
        {
            StatusCode = statusCode;
            Error = error;
        }

        public HttpStatusCode StatusCode { get; }

        public bool IsSuccessStatusCode => ((int)StatusCode >= 200) && (int)StatusCode <= 299;

        public ErrorDto Error { get; }

        public T Data { get; }

        public T GetData()
        {
            if (!IsSuccessStatusCode)
                throw new ApiHttpException(StatusCode, Error);

            if (Data == null && Error != null)
                throw new ApiHttpException(StatusCode, Error);

            return Data;
        }

        public void Assert() => 
            GetData();

        internal static ApiResult<T> NotFound() =>
            new ApiResult<T>(HttpStatusCode.NotFound, new ErrorDto { Message = "Not found", ErrorCode = ErrorCodes.NotFound, IsRetryable = false });

        internal static ApiResult<T> BadRequest() =>
            new ApiResult<T>(HttpStatusCode.BadRequest, new ErrorDto { Message = "Bad request", ErrorCode = ErrorCodes.BadRequest, IsRetryable = false });

        internal static ApiResult<T> BadRequest(string message) =>
            new ApiResult<T>(HttpStatusCode.BadRequest, new ErrorDto { Message = message, ErrorCode = ErrorCodes.BadRequest, IsRetryable = false });
    }
}
