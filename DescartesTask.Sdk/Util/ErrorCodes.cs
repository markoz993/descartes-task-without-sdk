﻿namespace DescartesTask.Sdk.Util
{
    public static class ErrorCodes
    {
        public const string AuthorityServiceUnavailable = "AUTHORITY_SERVICE_UNAVAILABLE";
        public const string AuthorityEndpointFailiure = "AUTHORITY_ENDPOINT_FAILIURE";
        public const string AuthorityUnableToGetToken = "AUTHORITY_UNABLE_TO_GET_TOKEN";
        public const string AuthorityUnableToFormatToken = "AUTHORITY_UNABLE_TO_FORMAT_TOKEN";

        public const string EndpointFailiure = "ENDPOINT_FAILIURE";
        public const string EndpointUnavailable = "ENDPOINT_UNAVAILABLE";
        public const string UnableToSerializeResponse = "UNABLE_TO_SERIALIZE_RESPONSE";

        public const string DatabaseError = "DATABASE_ERROR";

        public const string UnknownObjectType = "UNKOWN_OBJECT_TYPE";

        public const string NoContent = "NO_CONTENT";
        public const string BadRequest = "BAD_REQUEST";
        public const string NotFound = "NOT_FOUND";
        public const string NotAuthenticated = "NOT_AUTHENTICATED";
        public const string InternalServerError = "INTERNAL_SERVER_ERROR";
    }
}
