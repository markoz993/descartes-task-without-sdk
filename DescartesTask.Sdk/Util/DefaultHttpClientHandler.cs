﻿using System.Net.Http;
using System.Net;

namespace DescartesTask.Sdk.Util
{
    public class DefaultHttpClientHandler : HttpClientHandler
    {
        public DefaultHttpClientHandler()
        {
            AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

            #if !NET452
            MaxConnectionsPerServer = 25;
            #endif
        }
    }
}
