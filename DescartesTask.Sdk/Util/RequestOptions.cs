﻿using System.Threading;
using System;

namespace DescartesTask.Sdk.Util
{
    public class RequestOptions
    {
        public CancellationToken? CancellationToken { get; set; }
        public TimeSpan? RequestTimeout { get; set; }
        internal bool SkipToken { get; set; }

        internal void SetInternalProperties(TimeSpan? defaultTimeout)
        {
            RequestTimeout = RequestTimeout ?? defaultTimeout;
        }
    }
}
