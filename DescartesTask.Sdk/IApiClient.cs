﻿using DescartesTask.Sdk.Resources.Interfaces;

namespace DescartesTask.Sdk
{
    public interface IApiClient
    {
        IInputResource Inputs { get; set; }
    }
}
