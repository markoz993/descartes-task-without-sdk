﻿using DescartesTask.Sdk.Resources.Interfaces;
using DescartesTask.Sdk.Decompression;
using DescartesTask.Sdk.Formating;
using DescartesTask.Sdk.Resources;
using DescartesTask.Sdk.Contracts;
using DescartesTask.Sdk.Retry;
using DescartesTask.Sdk.Util;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Reflection;
using System.Threading;
using System.Net.Http;
using System.Linq;
using System.Net;
using System.Text;
using System.IO;
using System;

namespace DescartesTask.Sdk
{
    public class ApiClient : IApiClient
    {
        private readonly string _assemblyVersion;
        private readonly HttpClient _httpClient;
        private readonly TokenCallback _tokenHandler;
        private readonly IHttpFormatter _formatter;
        private readonly IDecompressor _decompressor;
        private readonly IRetryPolicy _retryPolicy;
        private readonly ICompressor _compressor;
        private readonly TimeSpan? _defaultRequestTimeout;
        public delegate Task<ApiResult<string>> TokenCallback();

        public ApiClient(HttpClient httpClient, ClientOptions options)
        {
            options = options ?? new ClientOptions();

            _httpClient = httpClient;
            _tokenHandler = options.GetToken;
            _assemblyVersion = typeof(ApiClient).GetTypeInfo().Assembly.GetName().Version.ToString();
            _formatter = options.Formatter ?? new JsonHttpFormatter();
            _decompressor = options.Decompressor;
            _compressor = options.Compressor;
            _retryPolicy = options.RetryPolicy ?? new RetryWithTimeSpanBackoff();
            _defaultRequestTimeout = options.DefaultRequestTimeout;

            if (_decompressor != null)
                _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Encoding", _decompressor.EncodingType);

            //Create resources
            Inputs = new InputResource(this);
        }

        public IInputResource Inputs { get; set; }


        #region Client HTTP Methods & Helpers
        internal async Task<ApiResult<T>> GetAsync<T>(RequestOptions options, string uri) =>
            await SendRequestWithRetryPolicyAsync<T>(HttpMethod.Get, options, uri, null);

        internal async Task<ApiResult<T>> PostAsync<T>(RequestOptions options, string uri, object payload) =>
            await SendRequestAsync<T>(HttpMethod.Post, options, uri, null, MapPayloadToContent(payload));

        internal async Task<ApiResult<T>> PutAsync<T>(RequestOptions options, string uri, object payload) =>
            await SendRequestWithRetryPolicyAsync<T>(HttpMethod.Put, options, uri, MapPayloadToContent(payload));

        internal async Task<ApiResult<T>> DeleteAsync<T>(RequestOptions options, string uri) =>
            await SendRequestWithRetryPolicyAsync<T>(HttpMethod.Delete, options, uri, null);

        private HttpContent MapPayloadToContent(object payload)
        {
            if (payload is HttpContent content)
                return content;

            return new StringContent(_formatter.SerializeObject(payload), Encoding.UTF8, "application/json");
        }

        private async Task<ApiResult<T>> SendRequestWithRetryPolicyAsync<T>(HttpMethod method, RequestOptions options, string uri, HttpContent requestContent)
        {
            options.SetInternalProperties(_defaultRequestTimeout);

            return await _retryPolicy.RunAsync(async () => { ApiResult<T> result = await SendRequestAsync<T>(method, options, uri, Guid.NewGuid().ToString(), requestContent); return result; });
        }

        private async Task<ApiResult<T>> SendRequestAsync<T>(HttpMethod method, RequestOptions options, string uri, string requestId, HttpContent requestContent)
        {
            requestId = requestId ?? Guid.NewGuid().ToString();

            var request = new HttpRequestMessage(method, uri);

            string bearerToken = null;
            if (!options.SkipToken && _tokenHandler != null)
            {
                var tokenResult = await _tokenHandler();
                if (!tokenResult.IsSuccessStatusCode)
                    return new ApiResult<T>(tokenResult.StatusCode, tokenResult.Error);

                bearerToken = tokenResult.Data ?? string.Empty;
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", bearerToken);
            }

            request.Content = requestContent;
            if (_compressor != null && requestContent != null && requestContent.Headers.ContentType.MediaType.Contains("application/json"))
                request.Content = _compressor.Compress(requestContent);

            request.Headers.Add("sdk-version", _assemblyVersion);

            AddDefaultRequestHeaders(request, requestId);

            try
            {
                using (var response = await SendAsync(request, options))
                {
                    //Handle errors
                    if (response.StatusCode == HttpStatusCode.ServiceUnavailable)
                    {
                        return new ApiResult<T>(HttpStatusCode.ServiceUnavailable, new ErrorDto
                        {
                            Message = "Service Unavailable: " + response.RequestMessage.RequestUri,
                            ErrorCode = ErrorCodes.EndpointUnavailable,
                            IsRetryable = true
                        });
                    }

                    if (response.StatusCode != HttpStatusCode.NoContent)
                    {
                        using (var responseStream = await response.Content.ReadAsStreamAsync())
                        {
                            if (_decompressor != null)
                            {
                                string contentEncoding = response.Content.Headers.ContentEncoding.FirstOrDefault();
                                if (_decompressor.EncodingType == contentEncoding)
                                {
                                    using (MemoryStream destination = new MemoryStream())
                                    {
                                        await _decompressor.Decompress(responseStream, destination);
                                        return ReadBody<T>(response, destination);
                                    }
                                }
                            }

                            return ReadBody<T>(response, responseStream);
                        }
                    }

                    return new ApiResult<T>(response.StatusCode, default(T));
                }
            }
            catch (HttpRequestException ex)
            {
                //Unable to reach service
                return new ApiResult<T>(HttpStatusCode.InternalServerError, new ErrorDto
                {
                    Message = $"Unable to call to {_httpClient.BaseAddress.ToString().TrimEnd('/') + "/" + uri.TrimStart('/')}. " + ex,
                    ErrorCode = ErrorCodes.EndpointFailiure,
                    IsRetryable = true
                });
            }
        }

        /// <summary>
        /// Function that can be overridden
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        protected virtual async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, RequestOptions options)
        {
            //Use cancellationToken specified in this request
            if (options.CancellationToken.HasValue)
                return await _httpClient.SendAsync(request, options.CancellationToken.Value);

            //Set cancellation token with request timeout or default request timeout
            if (options.RequestTimeout.HasValue)
                using (CancellationTokenSource source = new CancellationTokenSource(options.RequestTimeout.Value))
                {
                    return await _httpClient.SendAsync(request, source.Token);
                }

            //Make request with max timeout specified in HttpClient.Timeout
            return await _httpClient.SendAsync(request);
        }

        private ApiResult<T> ReadBody<T>(HttpResponseMessage response, Stream responseStream)
        {
            if (!response.IsSuccessStatusCode && responseStream.Length == 0)
            {
                return new ApiResult<T>(response.StatusCode, new ErrorDto
                {
                    Message = "No content found in response.",
                    ErrorCode = ErrorCodes.NoContent,
                    IsRetryable = false
                });
            }
            else if (response.IsSuccessStatusCode && responseStream.Length > 0)
            {
                try
                {
                    return new ApiResult<T>(response.StatusCode, _formatter.DeserializeObject<T>(responseStream));
                }
                catch (Exception ex)
                {
                    return new ApiResult<T>(response.StatusCode, new ErrorDto
                    {
                        Message = ex.Message,
                        ErrorCode = ErrorCodes.UnableToSerializeResponse,
                        StackTrace = ex.StackTrace,
                        IsRetryable = false
                    });
                }
            }
            else if (!response.IsSuccessStatusCode)
            {
                try
                {
                    return new ApiResult<T>(response.StatusCode, _formatter.DeserializeObject<ErrorDto>(responseStream));
                }
                catch (Exception ex)
                {
                    return new ApiResult<T>(response.StatusCode, new ErrorDto
                    {
                        Message = ex.Message,
                        ErrorCode = ErrorCodes.UnableToSerializeResponse,
                        StackTrace = ex.StackTrace,
                        IsRetryable = false
                    });
                }
            }

            return new ApiResult<T>(response.StatusCode, default(T));
        }

        protected virtual void AddDefaultRequestHeaders(HttpRequestMessage request, string requestId)
        {
            request.Headers.TryAddWithoutValidation("RequestId", requestId);
        }

        #endregion
    }
}
