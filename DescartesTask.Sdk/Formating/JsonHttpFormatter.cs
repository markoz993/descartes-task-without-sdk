﻿using Newtonsoft.Json;
using System.IO;

namespace DescartesTask.Sdk.Formating
{
    public class JsonHttpFormatter : IHttpFormatter
    {
        private readonly JsonSerializer _serializer;
        private readonly JsonSerializerSettings _settings;

        public JsonHttpFormatter(JsonSerializerSettings settings = null)
        {
            _serializer = JsonSerializer.Create(settings ?? new JsonSerializerSettings());
            _settings = settings;
        }

        public T DeserializeObject<T>(Stream responseStream)
        {
            using (StreamReader sr = new StreamReader(responseStream))
            {
                using (JsonReader reader = new JsonTextReader(sr))
                {
                    return _serializer.Deserialize<T>(reader);
                }
            }
        }

        public string SerializeObject(object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented, _settings);
        }
    }
}
