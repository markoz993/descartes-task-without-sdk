﻿using System.Net.Http;

namespace DescartesTask.Sdk.Decompression
{
    public class GZipCompressor : ICompressor
    {
        public string EncodingType => "gzip";

        public HttpContent Compress(HttpContent source)
        {
            return new CompressedContent(source, EncodingType);
        }
    }
}
