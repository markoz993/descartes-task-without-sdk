﻿using System.Net.Http;

namespace DescartesTask.Sdk.Decompression
{
    public interface ICompressor
    {
        string EncodingType { get; }

        HttpContent Compress(HttpContent source);
    }
}
