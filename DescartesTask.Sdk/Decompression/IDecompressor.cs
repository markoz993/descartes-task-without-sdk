﻿using System.Threading.Tasks;
using System.IO;

namespace DescartesTask.Sdk.Decompression
{
    public interface IDecompressor
    {
        string EncodingType { get; }

        Task Decompress(Stream source, Stream destination);
    }
}
