﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO.Compression;
using System.Net.Http;
using System.Net;
using System.IO;
using System;

namespace DescartesTask.Sdk.Decompression
{
    public class CompressedContent : HttpContent
    {
        private readonly HttpContent originalContent;
        private readonly string encodingType;

        public CompressedContent(HttpContent content, string encodingType)
        {
            if (encodingType == null)
            {
                throw new ArgumentNullException(nameof(encodingType));
            }

            originalContent = content ?? throw new ArgumentNullException(nameof(content));
            this.encodingType = encodingType.ToLowerInvariant();

            if (this.encodingType != "gzip" && this.encodingType != "deflate")
            {
                throw new InvalidOperationException($"Encoding '{this.encodingType}' is not supported. Only supports gzip or deflate encoding.");
            }

            foreach (KeyValuePair<string, IEnumerable<string>> header in originalContent.Headers)
            {
                Headers.TryAddWithoutValidation(header.Key, header.Value);
            }

            Headers.ContentEncoding.Add(encodingType);
        }

        protected override bool TryComputeLength(out long length)
        {
            length = -1;

            return false;
        }

        protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
        {
            Stream compressedStream = null;

            if (encodingType == "gzip")
            {
                compressedStream = new GZipStream(stream, CompressionMode.Compress, true);
            }
            else if (encodingType == "deflate")
            {
                compressedStream = new DeflateStream(stream, CompressionMode.Compress, true);
            }

            return originalContent.CopyToAsync(compressedStream).ContinueWith(tsk => {
                compressedStream?.Dispose();
            });
        }
    }
}
