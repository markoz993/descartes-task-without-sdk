﻿using DescartesTask.Sdk.Contracts;

namespace DescartesTask.Sdk.Resources.Interfaces
{
    public interface IInputResource
    {
        ApiRequest<DiffResultDto> GetDiff(int id);

        ApiRequest<bool> PutLeft(int id, string data);
        ApiRequest<bool> PutRight(int id, string data);
    }
}
