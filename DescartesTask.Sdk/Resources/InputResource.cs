﻿using DescartesTask.Sdk.Resources.Interfaces;
using DescartesTask.Sdk.Contracts;

namespace DescartesTask.Sdk.Resources
{
    public class InputResource : IInputResource
    {
        const string BASE_URL = "/v1/diff/";
        private readonly ApiClient _client;

        internal InputResource(ApiClient client)
        {
            _client = client;
        }

        public ApiRequest<DiffResultDto> GetDiff(int id)
        {
            return new ApiRequest<DiffResultDto>(async options => 
            { 
                var uri = $"{BASE_URL}/{id}"; 
                return await _client.GetAsync<DiffResultDto>(options, uri); 
            });
        }

        public ApiRequest<bool> PutLeft(int id, string data)
        {
            return new ApiRequest<bool>(async options =>
            {
                var uri = $"{BASE_URL}/{id}/left";
                return await _client.PutAsync<bool>(options, uri, data);
            });
        }

        public ApiRequest<bool> PutRight(int id, string data)
        {
            return new ApiRequest<bool>(async options =>
            {
                var uri = $"{BASE_URL}/{id}/right";
                return await _client.PutAsync<bool>(options, uri, data);
            });
        }
    }
}