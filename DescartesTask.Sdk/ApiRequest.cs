﻿using DescartesTask.Sdk.Util;
using System.Threading.Tasks;
using System.Threading;
using System;

namespace DescartesTask.Sdk
{
    public class ApiRequest<T>
    {
        private readonly Func<RequestOptions, Task<ApiResult<T>>> _func;
        private RequestOptions _options;

        public ApiRequest(Func<RequestOptions, Task<ApiResult<T>>> func)
        {
            _func = func;
        }

        public async Task<ApiResult<T>> ResultAsync() =>
            await _func(_options ?? new RequestOptions());

        public async Task<T> TryAsync() =>
            (await ResultAsync()).Data;

        public async Task<T> ExecAsync() =>
            (await ResultAsync()).GetData();

        public ApiRequest<T> SetCancellationToken(CancellationToken timeout)
        {
            _options = _options ?? new RequestOptions();
            _options.RequestTimeout = null;
            _options.CancellationToken = timeout;

            return this;
        }

        public ApiRequest<T> SetCancellationToken(TimeSpan? timeout)
        {
            _options = _options ?? new RequestOptions();
            _options.CancellationToken = null;
            _options.RequestTimeout = timeout;

            return this;
        }
    }
}
